## Rakuten Tv - Test Example
Rakuten TV is a video-on-demand streaming service, offering movies and TV series for subscription, rental and purchase
## Description
This is an example project for skill testing written in core react without using any kind of template (create-react-app) 
or framework(next.js) which uses webpack with babel to generate a build folder with the command of `npm run build` 
which contains files(index.html & index.js)

## Technologies & Languages
1. [Node.js = v15.5.0 and NPM = 7.3.0](https://nodejs.org/en/)
2. [React 17.0.1](https://reactjs.org/)
3. Advanced JavaScript
4. HTML5 & CSS3

## Getting Started
* First thing first, If you don’t have Node.js installed [install it from here](https://nodejs.org/en/). You’ll need Node.js version v15.5.0 or later.
* Clone project `git clone https://gitlab.com/mnifaf/rakutentv.git`
* Install dependencies `npm install --force --lagacy-peer-deps`
* Run the development server `npm run start`
* Create Production Build `npm run build`

```bash
Open [localhost.rakuten.tv] with your browser to see the result.
```

## Git Structure
* [Git Lab](https://gitlab.com/) use for this project
* Repository name is `rakutentv`
* Repo url is `https://gitlab.com/mnifaf/rakutentv.git` or `git@gitlab.com:mnifaf/rakutentv.git`
* `Master` is most stable branch for the production.

## Dependencies
* `npm install --force --lagacy-peer-deps` download all dependencies & will create a node_modules folder.
* These are production dependencies.
1. [react ^17.0.1](https://reactjs.org/)
2. [react-dom ^17.0.1](https://reactjs.org/docs/react-dom.html)
3. [localforage ^1.9.0](https://localforage.github.io/localForage/#localforage)
4. [react-redux: ^7.2.2](https://react-redux.js.org/)
5. [redux: ^4.0.5](https://redux.js.org/)
6. [react-router-dom: ^5.2.0](https://www.npmjs.com/package/react-router-dom)
7. [react-router: ^5.2.0](https://www.npmjs.com/package/react-router)
8. [bitmovin-player: ^3.23.0](https://www.npmjs.com/package/bitmovin-player)
9. [bitmovin-player-ui: "^3.23.0"](https://www.npmjs.com/package/bitmovin-player-ui)
10. [axios: "^0.21.1"](https://www.npmjs.com/package/axios) 
11. [@reduxjs/toolkit: "^1.5.0"](https://www.npmjs.com/package/@reduxjs/toolkit)

## Naming Convention
* Component Name e.g `ComponentName`
* Function Name e.g `functionName`
* Constant Name e.g `CONSTANT` or `CONSTANT_NAME` 
* Variable Name e.g `varName`

## Notes
* After clone & install all dependencies you should have 
* URL must have rakuten.tv as a main domain for appropriate API Call.
* You should have a `bitmovin Player KEY` associate with `rakuten.tv` domain.
* Put `export const PLAYER_KEY = "YOUR_PLAYER_KEY";` this statement in `src/constants/index` for Player. 

## Scripts
Core scripts
```bash
    "start": "webpack serve --mode development --open --hot",
    "build": "webpack --mode production",
    "test": "jest"
```

## Unit Test
* [JEST ^26.6.3](https://jestjs.io/docs/en/getting-started) will automatically install as a `dev` dependency to run unit test.
* To run unit test via jest use following command.
```bash
    npm run test
```

## Folder Structure
```
rakutentv
├── README.md
├── package.json
├── .babelrc
├── webpack.config   
├── .gitignore
├──  node_modules
├──  build
├── src
   │ index.html
   │ index.js           // Entry Point for webpack 
   ├── Assets           // Inlude images   
   ├── Component
   ├──      └── App.js  // Main Component
   ├── Config           // Contain system related to system (Cache etc)
   ├── Constant         // Systm Constant
   ├── Services         // Contin Service like (http Request Service, BrowserStorage, Cache Objec etc)
   ├── Store            // Contain all login related to Redux for state management
   ├── Test             // Unit test for functionality testing
   └── Utils            // Helper functions
 
```
## Code Overview
- `npm` used to install dependencies
- `webpack` used to crate build
- `src/ComponentsApp.js` is main component contains both `Home` and `movieDetail` component
- `redux` used to create an application state with a `@reduxjs/toolkit` to get better experience
- `axios` used to initiate a http request to API server 
- `localforage` used to get a better experience to save data in client storage
- `bitmovin-player` used to get play a stream
- `jest` used to automatically run unit test via `npm run test` command. All code exists in `src/test` folder

- `src/Services` This folder contains out service which import in multiple files.
     * `apiRequest` This module is responsible for http request working with `cacheService`
     * `cacheService` module which responsible to create/get cache of http response on client storage via using `browserStorage` module.
     *  Responsibility of `browserStorage` is to save/retrieve a cache file from client storage.

- `src/Store` All logic related to state management using with redux & redux-toolkit
    * `Store/configureStore.js` is responsible to create a store for application by using combine `reducer.js` file as well as `middleware` 
    * `Store/reducer.js` used to combine all reducer into to single place.
    * `Store/middleware/index.js` Contains all dispatch middleware including http Request middleware as well as default middleware for asynchronous dispatch.
    * `Store/slices` folder contain all the slices of store. slice contains both(action + reducer)
    * `Store/sliceHandlers` This import a specific slice from `Store/slice/slice_name` to add further wrapper which directly used with UI(userEvents(js,html,mouseEvents etc))
