const dev = {
  // API_CACHE
  API_CACHE_DURATIONS: {
    SHORT: 20 * 60, // 2 =  2min
    MEDIUM: 45 * 60, // 45min
    LONG: 3 * 24 * 60 * 60, // 3 days
  },
  API_CACHE_ENABLE: true,
};

export default dev;
