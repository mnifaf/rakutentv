import React, { Component, Fragment, Suspense } from "react";
import { Provider } from "react-redux";
import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
// create store
import store from "../Store/configureStore";
// import components
const Home = React.lazy(() => import("./Home"));
const MovieDetail = React.lazy(() => import("./MovieDetail"));
const state = store();
class App extends Component {
  render() {
    return (
      <Provider store={state}>
        <Suspense fallback={<div>Loading..</div>}>
          <BrowserRouter>
            <Switch>
              <Fragment>
                <Route exact path="/" component={Home}></Route>
                <Route
                  exact
                  path="/movieDetail/:id"
                  component={MovieDetail}
                ></Route>
              </Fragment>
            </Switch>
          </BrowserRouter>
        </Suspense>
      </Provider>
    );
  }
}
export default App;
