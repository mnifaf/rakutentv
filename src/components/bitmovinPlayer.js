import React, { useEffect, useState, useRef, Fragment } from "react";
import { PLAYER_KEY } from "../Constants";
import { Player } from "bitmovin-player";
import { UIFactory } from "bitmovin-player/bitmovinplayer-ui";
import "bitmovin-player/bitmovinplayer-ui.css";

const BitmovinPlayer = (props) => {
  const playerDiv = useRef();
  const [State, setState] = useState({ player: null, status: "off" });
  const playerConfig = {
    key: PLAYER_KEY,
  };
  const playerSource = {
    title: props.title,
    description: props.description,
    dash:
      "https://bitdash-a.akamaihd.net/content/MI201109210084_1/mpds/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.mpd",
    poster: props.poster,
  };

  useEffect(() => {
    setupPlayer();

    return () => {
      destroyPlayer();
    };
  }, []);

  const setupPlayer = () => {
    const player = new Player(playerDiv.current, playerConfig);
    UIFactory.buildDefaultUI(player);
    player.load(playerSource).then(
      () => {
        setState({
          ...State,
          player,
        });
        // player.play();

        // console.log("HasEnded", player.hasEnded());
        console.log("Successfully loaded source");
      },
      () => {
        console.log("Error while loading source");
      }
    );
  };

  const destroyPlayer = () => {
    if (State.player != null) {
      State.player.destroy();
      setState({
        ...State,
        player: null,
      });
    }
  };

  return (
    <Fragment>
      <div id="player" ref={playerDiv}></div>
    </Fragment>
  );
};

export default BitmovinPlayer;
