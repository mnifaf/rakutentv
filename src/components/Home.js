import React, { Component, Fragment } from "react";
import "./App.css";
import Logo from "../Assets/images/rakutentv-logo.png";
import { connect } from "react-redux";
import { addMovies } from "../store/sliceHandlers/moviesHandlers";

const Movie = React.lazy(() => import("./Movie"));

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movie: [],
    };
  }
  componentWillMount() {
    this.props.dispatch(addMovies());
  }

  render() {
    return (
      <Fragment>
        <div className="banner">
          <a href="" className="logoholder">
            <img src={Logo}></img>
          </a>
        </div>
        <div className="container">
          <div className="row">
            {this.props.movieCollection.length > 0 &&
              this.props.movieCollection.map((m, index) => {
                return <Movie key={index} obj={m} />;
              })}
          </div>
        </div>
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    movieCollection: state.movies.collection,
  };
}
export default connect(mapStateToProps)(Home);
