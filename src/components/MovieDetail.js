import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Logo from "../Assets/images/rakutentv-logo.png";
import api from "../Services/apiRequest";
import { API_BASE_URL } from "../Constants";

const BitmovinPlayer = React.lazy(() => import("./bitmovinPlayer"));

class MovieDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movieDetail: {},
      isLoaded: false,
    };
  }

  componentWillMount() {
    let movieDetail = this.props.movieCollection.find(
      (movie) => movie.id === this.props.match.params.id
    );
    if (movieDetail) {
      this.setState({
        movieDetail,
        isLoaded: true,
      });
    } else {
      let url =
        API_BASE_URL +
        `movies/${this.props.match.params.id}?classification_id=5&device_identifier=web&locale=es&market_code=es`;
      api.get(url).then((response) => {
        let res = response.data.data;
        let movieDetail = {
          images: {},
          classification: {},
          year: "",
        };
        movieDetail.images.snapshot = res.images.snapshot;
        movieDetail.images.artwork = res.images.artwork;
        movieDetail.classification.description = res.classification.description;
        movieDetail.year = res.year;
        this.setState({
          movieDetail,
          isLoaded: true,
        });
      });
    }
  }

  render() {
    if (this.state.isLoaded) {
      return (
        <div>
          <div
            className="banner sm"
            style={{
              backgroundImage: `url(${this.state.movieDetail.images.snapshot})`,
            }}
          >
            <div
              className="container"
              style={{ position: "relative", zindex: "1" }}
            >
              <Link to="/">
                <img className="logoholder sm" src={Logo}></img>
              </Link>
              <div className="row align-center">
                <div className="col-5">
                  <div className="movidetail">
                    <div
                      className="image"
                      style={{
                        backgroundImage: `url(${this.state.movieDetail.images.artwork})`,
                      }}
                    ></div>
                    <div className="desc">
                      <h3>Title</h3>
                      <p>{this.state.movieDetail.title}</p>
                      <h3>Description:</h3>
                      <p>{this.state.movieDetail.classification.description}</p>
                      <h3>Year:</h3>
                      <p>{this.state.movieDetail.year}</p>
                      <h3>Amount:</h3>
                      <p>{this.state.movieDetail.label}</p>
                    </div>
                  </div>
                </div>
                <div className="col-7">
                  <div id="player-wrapper">
                    {this.state.isLoaded ? (
                      <BitmovinPlayer
                        poster={this.state.movieDetail.images.snapshot}
                        title={this.state.movieDetail.title}
                        description={
                          this.state.movieDetail.classification.description
                        }
                      />
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <p> </p>
          </div>
          <div className="container"></div>
        </div>
      );
    } else {
      return <div>Loading...</div>;
    }
  }
}

function mapStateToProps(state) {
  return {
    movieCollection: state.movies.collection,
  };
}
export default connect(mapStateToProps)(MovieDetail);
