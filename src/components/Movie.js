import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const Movie = (props) => {
  return (
    <div className="col" data-test="movie-component">
      <div className="box">
        <div
          className="image"
          style={{
            backgroundImage: `url(${props.obj.images.artwork})`,
          }}
        >
          <Link
            to={`/movieDetail/${props.obj.id}`}
            className="stretch-link"
          ></Link>

          <div className="caption">
            <h4>{props.obj.title}</h4>
            <p>{props.obj.year}</p>
            <p>{props.obj.label}</p>
          </div>
        </div>
      </div>
    </div>
  );
};
Movie.propTypes = {
  obj: PropTypes.shape({
    title: PropTypes.string.isRequired,
    year: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    images: PropTypes.shape({
      artwork: PropTypes.string,
    }),
  }).isRequired,
};
export default Movie;
