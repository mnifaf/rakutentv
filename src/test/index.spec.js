import React from "react";
import ReactDOM from "react-dom";
import App from "../Components/App";

it("Index Should Render seamless", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
