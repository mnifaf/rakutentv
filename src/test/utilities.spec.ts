import globalUtilites from "../Utils/globalUtilities";

describe("globalUtilites", () => {
    it("isExpiredApi should return true in case of API cache time valid ", () => {
         let cacheTime =  ~~(Date.now() / 1000);

        let output = globalUtilites.cache.isExpiredApi(cacheTime - 110);
        expect(output).toEqual(true);
    });
        it("isExpiredApi should return false in case of API cache time is not valid ", () => {
         let cacheTime = ~~(Date.now() / 1000);

            let output = globalUtilites.cache.isExpiredApi(cacheTime + 500);
        expect(output).toEqual(false);
    });
});