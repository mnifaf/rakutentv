import checkPropTypes from "check-prop-types";

export const checkProps = (component, validProps) => {
  let result = checkPropTypes(
    component.propTypes,
    validProps,
    "prop",
    component.name
  );
  return result;
};

export const findByTestAttr = (component, divName) => {
  return component.find(`[data-test='${divName}']`);
};
