import BrowserStorage from "../Services/browserStorage";
import globalUtilites from "../Utils/globalUtilities";
describe("BrowserStorageService", () => {
  it("loalStore should return same data that will pass to the save function", () => {
    let currentTimeStamp = globalUtilites.cache.currentTimeStampInSecond(
      20 * 60
    );
    let response = { status: 200, body: { data: { name: "RakutenTv" } } };
    let resWithCacheTime = { x: currentTimeStamp, savedData: response };
    // Save data to localStorage
    BrowserStorage.localStorage.setWithJsonStringify(
      "apiName",
      resWithCacheTime
    );
    // retrive data form localStorage
    let output = BrowserStorage.localStorage.getWithJsonStringify("apiName");
    expect(output).toEqual(resWithCacheTime);
  });
  it("localForage should return same data that will pass to the save function", () => {
    let currentTimeStamp = globalUtilites.cache.currentTimeStampInSecond(
      20 * 60
    );
    let response = { status: 200, body: { data: { value: true } } };
    let resWithCacheTime = { x: currentTimeStamp, savedData: response };
    //  Save data via localForage
    BrowserStorage.localForage.setWithJsonStringify(
      "apiName",
      resWithCacheTime
    );
    // retrive data form localForage
    BrowserStorage.localForage
      .getWithJsonStringify("apiName")
      .then((output) => {
        expect(output).toEqual(resWithCacheTime);
      });
  });
});
