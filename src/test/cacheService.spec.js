import cacheService from "../Services/cacheService";
import config from "../Config";

const { API_CACHE_DURATIONS } = config;

describe("cacheService", () => {
  it("createCache first save to the case then same object should return ", () => {
    let response = { status: 200, body: { data: { name: "RakutenTv" } } };
    cacheService.createCache("cacheName", response, API_CACHE_DURATIONS.SHORT);
  });

  cacheService.isCacheExists("cacheName").then((output) => {
    expect(output).toEqual({
      doFetch: false,
      savedData: { status: 200, body: { data: { name: "RakutenTv" } } },
    });
  });
});
