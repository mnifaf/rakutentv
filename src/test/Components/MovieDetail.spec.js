import React from "react";
import MovieDetail from "../../Components/MovieDetail";
import { shallow, mount, render } from "enzyme";
import store from "../../Store/configureStore";
const setupWrapper = () => {
  const movieDetailWrapper = shallow(
    <MovieDetail
      store={store()}
      match={{
        params: { id: "pinocho-2019" },
        isExact: true,
        path: "movieDetail",
        url: "/",
      }}
    />
  )
    .dive()
    .dive();
  return movieDetailWrapper;
};
describe("MovieDetail Component Test", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setupWrapper();
  });
  it("MovieDetail Component should render seamless", () => {
    console.log(wrapper.debug());
    expect(wrapper).toBeTruthy();
  });
});
