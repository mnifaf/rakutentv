import React from "react";
import Movie from "../../Components/Movie";
import { shallow, mount, render } from "enzyme";
import { checkProps, findByTestAttr } from "../testUtils";
describe("Movie Component ", () => {
  let wrapper;
  const obj = {
    title: "Movie Test Title",
    year: "2021",
    label: "Test Label",
    images: {
      artwork:
        "https://d2sjzkge2kr555.cloudfront.net/movie/posters/original/movie_poster1.jpg",
    },
  };
  beforeEach(() => {
    wrapper = shallow(<Movie obj={obj} />);
  });
  it("render without error", () => {
    let div = findByTestAttr(wrapper, "movie-component");
    expect(div.length).toBe(1);
  });
  it("Movie Component should render seamless", () => {
    expect(wrapper).toBeTruthy();
  });
  it("valid props", () => {
    const propError = checkProps(wrapper, obj);
    expect(propError).toBeUndefined();
  });
});
