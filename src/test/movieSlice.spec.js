import {
  createPaylodForAddMoviesAction,
  addMovies,
} from "../Store/sliceHandlers/moviesHandlers";
import { apiCallBegan } from "../Store/apiActions";
import { moviesAdded } from "../Store/slices/movies";
import { HTTP_METHOD, API_BASE_URL } from "../Constants";
//store
import configureStore from "../Store/configureStore";

// test action. this will test our createPaylodForAddMoviesAction() FUNCTION.
describe("movieSlice actions & Reducer", () => {
  const store = configureStore();
  it("createPaylodForAddMoviesAction test fn should return define object to dispatch as an action with movieAdd", () => {
    const output = createPaylodForAddMoviesAction();
    const expected = {
      type: apiCallBegan.type,
      payload: {
        url:
          API_BASE_URL +
          "lists/estrenos-imprescindibles-en-taquilla?classification_id=5&device_identifier=web&locale=es&market_code=es",
        apiUrl: "movieCollection",
        method: HTTP_METHOD.GET,
        data: {},
        cache: true,
        onSuccess: moviesAdded.type,
      },
    };
    expect(output).toEqual(expected); // deep equal
  });

  it("movieAdded reducer should save data into movie slice of state", () => {
    const action = {
      type: moviesAdded.type,
      payload: [1, 2, 3, 4, 5],
    };
    store.dispatch(action).then((result) => {
      let movieCollectionInStat = store.getState().movies.collection;
      expect(movieCollectionInStat).toEqual(action.payload);
    });
  });
});
