let globalUtilities = (() => {
  return {
    cache: {
      isExpiredApi: (timestamp) => {
        if (timestamp) {
          let currentTimestamp = ~~(Date.now() / 1000);
          return currentTimestamp > timestamp;
        }
      },
      currentTimeStampInSecond: (addSecond = undefined) => {
        let currentTimeStamp = ~~(Date.now() / 1000); // ~~ means Math.floor;
        if (addSecond) {
          currentTimeStamp += addSecond;
        }
        return currentTimeStamp;
      },
    },
  };
})();

export default globalUtilities;
