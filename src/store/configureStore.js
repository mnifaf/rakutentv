import { configureStore } from "@reduxjs/toolkit";
import combinedReducers from "./reducer";
import middleware from "./middleware/index";

export default () => {
  // this fun come from toolkit. It has already included devToolsEnhancer(trace:true)
  // also included redux tool kit extension settings
  // it also allow us to dispatch async actions
  // under the hood redux-toolkit use immer.js for reducer to maintain immutability
  return configureStore({ reducer: combinedReducers, middleware });
};
