import { combineReducers } from "redux";
import movies from "./slices/movies";

export default combineReducers({
  movies,
});
