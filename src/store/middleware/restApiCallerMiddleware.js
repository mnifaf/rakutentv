import api from "../../Services/apiRequest";
import * as apiActions from "../apiActions";
import { HTTP_METHOD } from "../../Constants";

/**
 * Summary. restApiCallerMiddleware fn
 * Description. This is our second middleware which will run if action has type of `apiCallBegan`.
 * It will make a http request on the base of define param in payload. After getting response from server
 * it will dispatch another action to get a new state with new data.
 * @function restApiCallerMiddleware
 * @param {object} store     Description. Param will pass automatically.
 */
const restApiCallerMiddleware = ({ dispatch, getState }) => (next) => async (
  action
) => {
  if (action.type !== apiActions.apiCallBegan.type) return next(action);
  next(action);
  const {
    url,
    apiUrl,
    method,
    cache,
    data,
    onSuccess,
    onError,
  } = action.payload;
  console.log("request param in restApiCallerMiddleware");
  console.log(action.payload);
  let response = "";
  switch (method) {
    case HTTP_METHOD.GET:
      if (cache) {
        response = await api.cache.get(url, apiUrl);
      } else {
        response = await api.get(url);
      }
      break;
    case HTTP_METHOD.POST:
      if (cache) {
        response = await api.cache.post(url, apiUrl, data);
      } else {
        response = await api.post(url, apiUrl, data);
      }
      break;
    default:
  }
  console.log("response in restApiCallerMiddleware");
  console.log(response);
  if (response && response.status === 200) {
    if (onSuccess) {
      dispatch({ type: onSuccess, payload: response.data.data.contents.data });
    }
    return response;
  } else if (response && response.status === 422) {
    // 422 mean validation errors
    console.log("onError", onError);
    return response;
  } else if (response && response.status === 403) {
    // unauthorized
    console.log("unauthorized request");
  } else {
    console.log("CORS_ERROR");
  }
};
export default restApiCallerMiddleware;
