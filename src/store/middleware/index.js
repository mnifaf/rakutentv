import { getDefaultMiddleware } from "@reduxjs/toolkit";
import logger from "./logger";
import restApiCallerMiddleware from "./restApiCallerMiddleware";

const middleware = () => {
  return [
    ...getDefaultMiddleware(), // arrays of default middleware. Thunk middleware also included into it.
    logger({ destination: "rakutenTv" }),
    restApiCallerMiddleware,
  ];
};

export default middleware;
