/**
 * Summary. logger fn
 * Description. Simple function run as a middleware whenever action dispatch.It will log usefull data in console.
 * currying use in this function. It takes params automatically from store.
 * @function logger
 * @param {object} params     Description. This oject had been passed when middle attatch with store.
 * @param {Object} store     Description. Store will pass automatically.
 * @param {Object} next     Description. Next middleware.
 * @param {Object} action    Description. Which action is currently in process.
 */
const logger = (params) => (store) => (next) => (action) => {
  // const logger = params => ({getStore , dispatch}) => next => action => {
  console.log(
    " >>> logger:params <<< ",
    params.destination,
    " >>> logger:store <<< ",
    store,
    " >>> logger:next <<< ",
    next,
    " >>> logger:action <<< ",
    action
  );
  return next(action);
};

export default logger;
