// Slice is a Ducks Pattern
import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "movies",
  initialState: {
    collection: [],
  },
  reducers: {
    /**
     * Summary. moviesAdded fn
     * Description. Movies Slice will return an action and reducer with this fn name `moviesAdded` to dispatch paylod to upate state. It is an async funtion.
     * @function moviesAdded
     * @param {Object} moives     Description. Latest State.
     * @param {Object} action     Description. Action object with type and payload.
     */
    moviesAdded: (movies, action) => {
      movies.collection = action.payload;
    },
  },
});

export const { moviesAdded } = slice.actions;
export default slice.reducer;
