import { createAction } from "@reduxjs/toolkit";

/**
 * Summary. apiCallBegan fn
 * Description. This action create use to allow middleware to make http request.
 * @function apiCallBegan
 * @return {type} Return action object with type of api/callBegan.
 */
export const apiCallBegan = createAction("api/callBegan");
