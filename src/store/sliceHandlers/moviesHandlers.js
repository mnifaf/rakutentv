import * as apiActions from "../apiActions";
import { HTTP_METHOD, API_BASE_URL } from "../../Constants";
import * as moviesSliceActions from "../slices/movies";

/**
 * Summary. addMovies fn
 * Description. This is use to create a action with appropriate payload to fetch http req
 * then it will put data into the state.
 * @function addMovies
 * @param {Object} data     Description. This is a collection of movies come form server via api.
 * @return {type} Return action create wrappe with dispatch.
 */
export const addMovies = () => (dispatch, getState) => {
  return dispatch(createPaylodForAddMoviesAction());
};

/**
 * Summary. createPaylodForAddMoviesAction fn
 * Description. Create a payload using apiCallBegan to enter http request middleware before dispatch data..
 * @function createPaylodForAddMoviesAction
 * @param {Object} data     Description. This is a collection of movies come form server via api.
 * @return {type} Return an action create.
 */
export const createPaylodForAddMoviesAction = () => {
  return apiActions.apiCallBegan({
    url:
      API_BASE_URL +
      "lists/estrenos-imprescindibles-en-taquilla?classification_id=5&device_identifier=web&locale=es&market_code=es",
    apiUrl: "movieCollection",
    method: HTTP_METHOD.GET,
    data: {},
    cache: true,
    onSuccess: moviesSliceActions.moviesAdded.type,
    // onError : (error) => { console.log("error show in action Creator"); console.log(error); }
  });
};
