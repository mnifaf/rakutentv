import axios from "axios";
import cacheService from "./cacheService";
import config from "../Config";
import { HTTP_METHOD } from "../Constants";
// const globalHeaders = {Accept: 'application/json ; charset=UTF-8','Content-Type': 'application/json'}

const { API_CACHE_DURATIONS } = config;

/**
 * Description. apiRequestWithCache this function responsible to return response from storage. If there is no saved data or time limit is exceed. Then Request will run to API server and response will save into browser cache;
 * @param {url} url     Description. This is a actual api url endpint.
 * @param {string} apiUrl     Description. Respoinse will save with this name if you set TRUE in cache flag.
 * @param {string} cacheDuration     Description. Default cache duration is 2 min but you can customize it.
 * @param {string} method Description. This is a http method like "GET" || "POST"
 * @param {object} data  Description. Request body contain some data in case of POST Request.
 * @return {type} Saved API Response.
 */
async function apiRequestWithCache(
  url,
  apiUrl,
  cacheDuration,
  method,
  data = {}
) {
  const dataFromCache = await cacheService.isCacheExists(apiUrl);
  const { doFetch, savedData } = dataFromCache;
  console.log(dataFromCache);
  if (doFetch) {
    let response = await apiRequest(url, method, data);
    if (response.status === 200) {
      let res = { status: 200, response };
      cacheService.createCache(apiUrl, res, cacheDuration);
    }
    return response;
  } else {
    return savedData.response;
  }
}

/**
 * Description. apiRequest This is a core method which has an axios api call. This method is responsible for hit a http request to API server.
 * @param {url} url     Description. This is a actual api url endpint.
 * @param {string} method Description. This is a http method like "GET" || "POST"
 * @param {object} data  Description. Request body contain some data in case of POST Request.
 * @return {type} API Response will return.
 */
async function apiRequest(url, method, data = {}) {
  console.log("apiRequest engine");
  let rawResponse = "";
  try {
    switch (method) {
      case HTTP_METHOD.GET:
        console.log("get request");
        rawResponse = await axios.request({ url, method });
        return rawResponse;
      case HTTP_METHOD.POST:
        rawResponse = await axios.request({ url, method, data });
        return rawResponse;
      default:
        console.log("Method not defined in api request");
    }
  } catch (error) {
    return error.response;
  }
}

/**
 * Description. API is an custom object wrapper which is responsible to wrap actually API request functionality into cache server.
 * you can use api.get or api.post to fetch data directly form server.
 * If you put cache work above api like (api.cache.get || api.cache.post) then it will check if data exists is broswer storage then return form storage otherwise it will call api and saved data into browser storage.
 * @param {string} apiName     Description. This is a name of api call. Api save in browser storage with this name.
 * @return {type} Saved API Response.
 */
const api = (() => {
  return {
    get: async (url) => {
      return apiRequest(url, HTTP_METHOD.GET);
    },
    post: async (url, data) => {
      return apiRequest(url, HTTP_METHOD.POST, data);
    },
    cache: {
      get: async (url, apiUrl, cacheDuration = API_CACHE_DURATIONS.SHORT) => {
        return await apiRequestWithCache(
          url,
          apiUrl,
          cacheDuration,
          HTTP_METHOD.GET
        );
      },
      post: async (
        url,
        apiUrl,
        data,
        cacheDuration = API_CACHE_DURATIONS.SHORT
      ) => {
        return apiRequestWithCache(
          url,
          apiUrl,
          cacheDuration,
          HTTP_METHOD.POST,
          data
        );
      },
    },
  };
})();

export default api;
