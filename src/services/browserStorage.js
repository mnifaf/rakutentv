import localforage from "localforage";
/**
 * Description. BrowserStorage is an custom object written to entertain Storage of browser related to Local, Session, IndexedDB, Cookie.
 * localForage use to save data in IndexedDB
 */
let BrowserStorage = (() => {
  return {
    localForage: {
      setWithJsonStringify: (key, value) => {
        let data = JSON.stringify(value);
        localforage.setItem(key.trim(), data).then(() => {
          return true;
        });
      },
      getWithJsonStringify: async (key) => {
        let data = await localforage.getItem(key.trim()).then((data) => {
          return data;
        });
        if (data) {
          return JSON.parse(data);
        } else {
          return data;
        }
      },
    },
    localStorage: {
      setWithJsonStringify: (key, value) => {
        let data = JSON.stringify(value);
        window.localStorage.setItem(key.trim(), data);
      },
      getWithJsonStringify: (key) => {
        let data = window.localStorage.getItem(key.trim());
        if (data) {
          return JSON.parse(data);
        } else {
          return data;
        }
      },
    },
  };
})();

export default BrowserStorage;
