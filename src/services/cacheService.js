import BrowserStorage from "./browserStorage";
import globalUtilites from "../Utils/globalUtilities";

export default {
  /**
   * Description. IsCacheExists take api name to find either api name is exists in browserStorage or not.
   * BrowserStorage is an object which responsible for saveing data into browser.
   * if you change (BrowserStorage.localStorage to BrowserStorage.localForage) then it will save data to browser storage.
   * @param {string} apiName     Description. This is a name of api call. Api save in browser storage with this name.
   * @return {type} Saved API Response.
   */
  async isCacheExists(apiName) {
    let dataFromCache = await BrowserStorage.localForage.getWithJsonStringify(
      apiName
    );
    if (dataFromCache && !globalUtilites.cache.isExpiredApi(dataFromCache.x)) {
      return { doFetch: false, savedData: dataFromCache.savedData };
    } else {
      return { doFetch: true, savedData: {} };
    }
  },
  /**
   * Description. createCache take api name to save data in broser storage(LocalStore,SessionStorage,IndexedDB)
   * It introduce a veriable named x for calculation of expireTime.
   * @param {string} apiName     Description. This is a name of api call. Api save in browser storage with this name.
   * @param {string} response     Description. This is the response that will be saved.
   * @param {string} expireTime     Description. This is basically a cache invalid time.
   * @return {type} Saved API Response to browser storages..
   */
  async createCache(apiName, response, expireTime) {
    let currentTimeStamp = globalUtilites.cache.currentTimeStampInSecond(
      expireTime
    );
    let resWithCacheTime = { x: currentTimeStamp, savedData: response };
    BrowserStorage.localForage.setWithJsonStringify(apiName, resWithCacheTime);
  },
};
