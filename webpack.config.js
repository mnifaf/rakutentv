const path = require("path"); // core node js module
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./src/index.js",
  output: {
    path: path.join(__dirname, "build"),
    filename: "index.js",
    publicPath: "/",
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(jpg|png)$/,
        use: {
          loader: "url-loader",
        },
      },
    ],
  },
  resolve: {
    extensions: ["*", ".js", ".jsx"],
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
  ],
  performance: {
    maxEntrypointSize: 5000000,
    maxAssetSize: 1000000,
  },
  devServer: {
    contentBase: path.join(__dirname, "build"),
    compress: true,
    public: "localhost.rakuten.tv",
    allowedHosts: ["localhost.rakuten.tv"],
    historyApiFallback: true,
  },
  //   devServer: {
  //     historyApiFallback: true,
  //     contentBase: path.join(__dirname, "build"),
  //   },
};
